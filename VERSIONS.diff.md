**Stable Updates Published**

* [Cactus Comments Client](https://gitlab.com/cactus-comments/cactus-client): [2.32.1](https://gitlab.com/cactus-comments/cactus-client/-/tags/2.32.1) -> [2.32.2](https://gitlab.com/cactus-comments/cactus-client/-/tags/2.32.2)
* [Element](https://github.com/element-hq/element-web): [v1.11.73](https://github.com/element-hq/element-web/releases/tag/v1.11.73) -> [v1.11.74](https://github.com/element-hq/element-web/releases/tag/v1.11.74)
* [Etherpad](https://github.com/ether/etherpad-lite): [2.1.1](https://github.com/ether/etherpad-lite/releases/tag/2.1.1) -> [2.2.2](https://github.com/ether/etherpad-lite/releases/tag/2.2.2)
* [Grafana](https://github.com/grafana/grafana): [11.1.3](https://github.com/grafana/grafana/releases/tag/v11.1.3) -> [11.1.4](https://github.com/grafana/grafana/releases/tag/v11.1.4)
* [Heisenbridge](https://github.com/hifi/heisenbridge): [1.14.6](https://github.com/hifi/heisenbridge/releases/tag/1.14.6) -> [1.15.0](https://github.com/hifi/heisenbridge/releases/tag/1.15.0)
* [Prometheus](https://github.com/prometheus/prometheus): [v2.53.1](https://github.com/prometheus/prometheus/releases/tag/v2.53.1) -> [v2.54.0](https://github.com/prometheus/prometheus/releases/tag/v2.54.0)
* Static Files: 2.32.1 -> 2.32.2
* [Synapse](https://github.com/element-hq/synapse): [v1.112.0](https://github.com/element-hq/synapse/releases/tag/v1.112.0) -> [v1.113.0](https://github.com/element-hq/synapse/releases/tag/v1.113.0)
* [Vaultwarden](https://github.com/dani-garcia/vaultwarden): [1.31.0](https://github.com/dani-garcia/vaultwarden/releases/tag/1.31.0) -> [1.32.0](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.0)
