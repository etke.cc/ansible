* Alertmanager Receiver: 2024.7.3
* Appservice Discord: v4.0.0
* Appservice Draupnir For All: 1.87.0
* Appservice Irc: 1.0.1
* Appservice Kakaotalk: 86c038fd2ffee5e0aebf65136f085cce7e38b54e
* Appservice Slack: 2.1.2
* Appservice Webhooks: v1.0.3-01
* Borg: 1.2.8
* Borgmatic: 1.8.13
* Buscarron: v1.4.2
* Cactus Comments: 0.9.0
* Cactus Comments Client: 2.32.2
* Chatgpt: 3.1.4
* Cinny: v4.1.0
* Container Socket Proxy: 0.2.0
* Corporal: 3.0.0
* Coturn: 4.6.2-r11
* Dimension: latest
* Discord: v0.7.0
* Docker Compose: v2.11.1
* Draupnir: v1.87.0
* Dynamic Dns: 3.11.2
* Element: v1.11.74
* Email2Matrix: 1.1.0
* Etherpad: 2.2.2
* Exim Relay: 4.98-r0-1
* Facebook: v0.5.1
* Firezone: 0.7.36
* Funkwhale: 1.4.0
* Gmessages: v0.4.3
* Go Neb: latest
* Go Skype Bridge: latest
* Googlechat: v0.5.2
* Gotosocial: 0.16.0
* Grafana: 11.1.4
* Hangouts: latest
* Heisenbridge: 1.15.0
* Honoroit: v0.9.24
* Hookshot: 5.4.1
* Hydrogen: v0.4.1
* Instagram: v0.3.1
* Jitsi: stable-9646
* Jitsi Ldap: 3
* Jitsi Prosody Auth Matrix User Verification Repo: 2839499cb03894d8cfc3e5b2219441427cb133d8
* Keydb: 6.3.4
* Languagetool: 6.4
* Ldap Registration Proxy: 296246afc6a9b3105e67fcf6621cf05ebc74b873
* Linkding: latest
* Linkedin: latest
* Ma1Sd: 2.5.0
* Matrix Registration Bot: 1.3.0
* Matrix Reminder Bot: v0.3.0
* Maubot: v0.4.2
* Meta Instagram: v0.3.2
* Meta Messenger: v0.3.2
* Miniflux: 2.1.4
* Mjolnir: v1.6.5
* Mx Puppet Discord: v0.1.1
* Mx Puppet Groupme: 533cccc8
* Mx Puppet Instagram: latest
* Mx Puppet Slack: v0.1.2
* Mx Puppet Steam: latest
* Mx Puppet Twitter: latest
* Ntfy: v2.10.0
* Pantalaimon: 0.10.5
* Peertube: v6.2.1
* Postmoogle: v0.9.20
* Prometheus: v2.54.0
* Prometheus Nginxlog Exporter: v1.10.0
* Prometheus Node Exporter: v1.8.2
* Prometheus Postgres Exporter: v0.14.0
* Radicale: 3.2.2.0
* Rageshake: 1.13.0
* Redis: 7.2.5
* Registration: v0.7.2
* Schildichat: v1.11.36-sc.3
* Signal: v0.6.3
* Slack: latest
* Sliding Sync: v0.99.19
* Sms Bridge: 0.5.9
* Spam Checker Mjolnir Antispam Git: v1.6.4
* Spam Checker Synapse Simple Antispam Git: 5ab711971e3a4541a7a40310ff85e17f8262cc05
* Static Files: 2.32.2
* Sygnal: v0.15.0
* Synapse: v1.113.0
* Synapse Admin: 0.10.3
* Synapse Auto Accept Invite: 1.1.3
* Synapse Auto Compressor: v0.1.4
* Synapse Reverse Proxy Companion: 1.27.0-alpine
* Synapse S3 Storage Provider: 1.3.0
* Telegram: v0.15.2
* Traefik: v3.1.2
* Traefik Certs Dumper: v2.8.3
* Twitter: v0.1.8
* Uptime Kuma: 1.23.13
* User Verification Service: v3.0.0
* Vaultwarden: 1.32.0
* Wechat: 0.2.4
* Wechat Agent: 0.0.1
* Whatsapp: v0.10.9
* Wsproxy: latest
* Wsproxy Syncproxy: latest
